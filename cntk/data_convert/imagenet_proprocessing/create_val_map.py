import os

id_file = "ILSVRC2012_validation_ground_truth.txt"
folder = "/scratch/snx3000/rengglic/data_cntk/validation";
val_map = "val_map.txt";

ids = [];
with open(id_file) as f:
    for line in f:
        ids.append(int(line));

with open(val_map, 'w') as f:
    print("Processing folder: {0}".format(folder));
    for img in os.listdir(folder):
        print(img);
        imgId = int(img.rsplit(".",1)[0].rsplit("_",1)[1]) - 1;
        f.write("{0}/{1}\t{2}\n".format(folder, img, ids[imgId]-1));
