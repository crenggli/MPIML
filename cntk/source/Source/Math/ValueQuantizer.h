#pragma once
#ifndef __VALLUE_QUANTIZER_H__
#define __VALLUE_QUANTIZER_H__

#include "Basics.h"
#include "BestGpu.h" // for CPUONLY
#ifndef CPUONLY
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#endif // !CPUONLY

#include <cassert>
#include <stdexcept>

#pragma warning(disable : 4127) // conditional expression is constant

namespace Microsoft { namespace MSR { namespace CNTK {

#ifdef __device__                          // this can be used in CUDA; if this is not defined, then we are compiling in a non-CUDA context
#define cudacode __device__                // CUDA: we assume we ONLY run these functions on CUDA (otherwise we'd need to mess with specifiers of matrixref)
#define cudasharedcode __device__ __host__ // shared on both CUDA and CPU; note that such functions cannot call into __device__ only functions like matrixref::operator(,)
#undef assert
#define assert(c)
#else
#define cudacode // non-CUDA context: defines to nothing
#define cudasharedcode
//#define QUANTUSEPPL
#endif

#ifdef QUANTUSEPPL
#include <ppl.h> // in non-CUDA: also use PPL lib
#endif

template <typename ElemType>
class QuantizedWordHelper;

template <>
class QuantizedWordHelper<float>
{
public:
    typedef unsigned int ValueType;
    typedef int ValueTypeSigned;
    static_assert(sizeof(float) == sizeof(ValueType), "Quantized word size != size of ElemType=float");
};

template <>
class QuantizedWordHelper<double>
{
public:
    typedef unsigned long long ValueType;
    typedef long long ValueTypeSigned;
    static_assert(sizeof(double) == sizeof(ValueType), "Quantized word size != size of ElemType=double");
};

#pragma warning(disable : 4334) // 'operator' : result of 32-bit shift implicitly converted to 64 bits (was 64-bit shift intended?)
template <class ElemType>
class ValueQuantizer
{
public:
    typedef typename QuantizedWordHelper<ElemType>::ValueType QWord;
    typedef typename QuantizedWordHelper<ElemType>::ValueType QWordVal;
    typedef typename QuantizedWordHelper<ElemType>::ValueTypeSigned QWordValSigned;
    static const size_t QWordNumBits = 8 * sizeof(QWord);

public:
    cudasharedcode ValueQuantizer(size_t ldNbits, ElemType scalingFactor)
        : ldNbits(ldNbits), Nbits(1 << ldNbits), quantimin((ElemType) 0.0), quantimax(scalingFactor)
    {
        // 1 bit is left for sign!
        rangeend = ((QWordVal) 1) << (Nbits - 1);

        // post-fix for incorrect shift for no-quant hack (Nbits=32): << arg is taken mod 32!
        // in this case, it's only used as (rangeend-1) which is now correct (before it was 0!)
        if (Nbits >= (8 * sizeof(rangeend)))
        {
            rangeend = 0;
        }

        // must protect against NaN: interval is 0 -> quantization is futile, just emit 0
        if (((quantimax - quantimin) < 1e-36f) || (rangeend == 0))
        {
            quantimax = (ElemType) 0.0;
        }
    }

    // quantize one value
    // TODO: we can optimize for 1 bit here - very simply use a template arg 'isonebit'
    template <bool ZeroThresholdFor1Bit>
    cudasharedcode QWordVal Quantize(ElemType u, float randomNumber = 0.0) const
    {
        if (Nbits == QWordNumBits)
        {
            return QuantizeToFullQWord(u);
        }
        else if (Nbits == 1)
        {
            return QuantizeOneBit(u, randomNumber);
        }
        else
        {
            if (quantimax < 1e-36f) {
                return (QWordVal) 0;
            }

            ElemType absU = abs(u);

            // we divide interval [0, scalingFactor] into (2 ^ (bits - 1) - 1) parts
            ElemType partLength = quantimax / (ElemType) (rangeend - (QWordVal) 1);
            QWordVal l = (QWordVal) (absU / partLength);

            // we need to map u either to l or (l+1)
            // we will use randomization in order to do that
            // so in expectation we will get unbiased estimator
            ElemType diff = absU - l * partLength;
            ElemType ratio = diff / partLength;

            // check if we should map it to l or (l+1)
            // based on randomNumber
            QWordVal mappingTo = l;
            if ( randomNumber > (ElemType) 1 - ratio ) {
                mappingTo++;
            }
            
            // add first bit to be 1 if sign is positive
            if ( u >= (ElemType) 0 ) {
                mappingTo |= rangeend;
            }

            return mappingTo;
        }
    }

    // unquantize one value
    cudasharedcode ElemType Unquantize(QWordVal u) const
    {
        // special branch that does not quantize at all, for testing
        if (Nbits == QWordNumBits)
        {
            return *(ElemType*) &u;
        }

        if (Nbits == 1)
        {
            return quantimax * (1.0 - (2.0 * (1.0 - u)));
        }

        // check if it is positive and erase first (sign) bit
        bool positive = (u & rangeend) > 0;
        if (positive) {
            u ^= rangeend;
        }

        ElemType value = u * (quantimax / (ElemType) (rangeend - (QWordVal) 1));
        if (!positive) {
            value *= (ElemType) -1.0;
        }

        return value;
    }

    // how many bits we are quanatizing to
    cudasharedcode size_t NBits() const
    {
        return Nbits;
    }

    // max value of quantize value; 2^Nbits
    cudasharedcode QWordVal QuanRangeEnd() const
    {
        return rangeend << 1;
    }

    // helper: compute the binary log of a power of two (utility function to convert 'Nbits' into 'ldNbits'
    static size_t ld(size_t v)
    {
        if (v == 1)
        {
            return 0;
        }
        else if (v & 1) // not a power of two
        {
            RuntimeError("ld: 'bits' must be a power of two");
        }
        else
        {
            return 1 + ld(v >> 1);
        }
    }

protected:
    // quantize for full ElemType size bits case (special case that allows to bypass quantization, for testing/debugging purposes)
    cudasharedcode QWordVal QuantizeToFullQWord(ElemType u) const
    {
        assert(Nbits == QWordNumBits);

        // we return the bit pattern that encodes the float value
        return *(QWordVal*) &u;
    }

    cudasharedcode QWordVal QuantizeOneBit(ElemType u, float randomNumber) const
    {
        assert(Nbits == 1);

        if (quantimax < 1e-36f) return 0.0;

        ElemType percentage = (quantimax - u) / (2.0 * quantimax);
        if (percentage > randomNumber)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

protected:
    // NBits must be power of two
    size_t ldNbits;
    size_t Nbits;

    QWordVal rangeend;

    // quantization range
    ElemType quantimin;
    ElemType quantimax;
};
}
}
}

#endif // __VALUE_QUANTIZER_H__
