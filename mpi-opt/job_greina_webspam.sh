#!/bin/sh

MACHINEFILE="nodes.$SLURM_JOB_ID"

# Generate Machinefile for mpich such that hosts are in the same
#  order as if run via srun
#
srun -l /bin/hostname | sort -n | awk '{print $2}' > $MACHINEFILE

echo "Start: $(date)"
echo "SLURM_NTASKS: $SLURM_NTASKS"
echo "-----------------------------"

epoch=10
stepinitial=1
step_decay=0.95
beta=0.55
training=./data/webspam_wc_normalized_trigram_shuffled.bin
dimension=16609144
testing=./data/temp_meta.bin
meta=./data/temp.meta

batchsizes=( 1000 )
progs=( bin/LOGIT_ALLREDUCE_EXPBACKOFF_STEPSIZES_SPARSE bin/LOGIT_ALLREDUCESPARSEBIG_EXPBACKOFF_STEPSIZES_SPARSE bin/LOGIT_ALLREDUCESPARSESMALL_EXPBACKOFF_STEPSIZES_SPARSE bin/LOGIT_ALLREDUCESPARSERECDBL_EXPBACKOFF_STEPSIZES_SPARSE bin/LOGIT_ALLREDUCESPARSERING_EXPBACKOFF_STEPSIZES_SPARSE )

for b in "${batchsizes[@]}"; do
  for p in "${progs[@]}"; do
    set -x
    # Run using generated Machine file:
    cmd="$p --binary 1 --dimension $dimension --beta $beta --epoch $epoch --batch_size $b --stepinitial $stepinitial --step_decay $step_decay --quantization 0 --qlevel 1 --splits 1 $training $testing $meta"
    echo $cmd
    $1 -np $SLURM_NTASKS -machinefile $MACHINEFILE $cmd
    set +x
  done
done

rm $MACHINEFILE

echo "-----------------------------"
echo "End: $(date)"
